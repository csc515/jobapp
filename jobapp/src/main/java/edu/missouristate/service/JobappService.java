package edu.missouristate.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.missouristate.domain.Application;
import edu.missouristate.repository.JobappRepository;

@Service("jobappService")
public class JobappService {

	@Autowired
	JobappRepository jobappRepo;
	
	public List<Application> getApplications() {
		return (List<Application>) jobappRepo.findAll();
	}
	
	// Save Application
	@Transactional
	public void saveApplications(Application application) {
		jobappRepo.save(application); // id == null; INSERT INTO applications(id, first_name...)
	}
	
	// Edit Application
	@Transactional
	public void editApplications(Application application) {
		jobappRepo.save(application);// id!=null; UPDATE applications SET... WHERE id = application.getId();
	}
	
	// Delete Application
	@Transactional
	public void deleteApplications(Application application) {
		jobappRepo.delete(application);
	}
	
	// Delete Application
	@Transactional
	public void deleteApplications(Integer id) {
		Application application = jobappRepo.findById(id).get();
		jobappRepo.delete(application);
	}
	
}
