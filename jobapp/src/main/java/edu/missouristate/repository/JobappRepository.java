package edu.missouristate.repository;

import org.springframework.data.repository.CrudRepository;

import edu.missouristate.domain.Application;

public interface JobappRepository extends CrudRepository<Application, Integer>, JobappRepositoryCustom {
	// Spring Data abstract methods go here
	// public Application findByFirstName(String firstName); // select * from applications where first_name = firstName 
}
