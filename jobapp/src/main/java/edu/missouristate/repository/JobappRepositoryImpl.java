package edu.missouristate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import edu.missouristate.domain.Application;
import edu.missouristate.domain.QApplication;

@Repository
public class JobappRepositoryImpl extends QuerydslRepositorySupport implements JobappRepositoryCustom {
	
	QApplication applicationTable = QApplication.application;
	
	public JobappRepositoryImpl() {
		super(Application.class);
	}

	// Object Oriented SQL goes here...
	public Application getApplicationById(Integer id) {
		return (Application) from(applicationTable)
				.where(applicationTable.id.eq(id))
				.limit(1)
				.fetch();
	}
	
	public List<Application> getBootListByFirstName(String firstName) {
		return from(applicationTable)
				.where(applicationTable.firstName.eq(firstName))
				.orderBy(applicationTable.id.asc())
				.fetch();
	}
	
}
