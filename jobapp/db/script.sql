CREATE database jobapp;

USE jobapp;

CREATE TABLE applications (
	id INTEGER NOT NULL AUTO_INCREMENT,
	first_name VARCHAR(64),
	last_name VARCHAR(64),
	PRIMARY KEY(id)
);