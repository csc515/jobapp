package edu.missouristate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import edu.missouristate.domain.Application;
import edu.missouristate.service.JobappService;

@Controller
public class JobappController {

	@Autowired
	JobappService jobappService;
	
	@GetMapping(value="/")
	public String getIndex(Model model) {
		List<Application> appList = jobappService.getApplications();
		model.addAttribute("appList", appList);
		return "index";
	}
	
	@GetMapping(value="/addApplication")
	public String getAddApplication(Model model) {
		
		return "addApplication";
	}
	
	@PostMapping(value="/addApplication")
	public String postAddApplication(Model model, @RequestBody Application application) {
		jobappService.saveApplications(application);
		return "redirect:/";
	}
	
	@GetMapping(value="/sw.js")
	public String getSw(Model model) {
		return "";
	}
}
